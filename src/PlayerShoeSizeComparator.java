import java.util.Comparator;

public class PlayerShoeSizeComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {
        //return (int) (o1.getShoeSize()-o2.getShoeSize());

        return Double.compare(o1.getShoeSize(), o2.getShoeSize());
    }
}
