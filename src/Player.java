public class Player implements Comparable{
    String name, surname, clubName="Afc";
    Integer overal, age;
    Float shoeSize=11.5f;

    public Player(String name, String surname, Integer overal, Integer age) {
        setName(name);
        setSurname(surname);
        setOveral(overal);
        setAge(age);
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public Float getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(Float shoeSize) {
        this.shoeSize = shoeSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getOveral() {
        return overal;
    }

    public void setOveral(Integer overal) {
        this.overal = overal;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        return getOveral()-((Player)o).getOveral();
        //return ((Player)o).getOveral()-getOveral();
        //return 1;
    }

    @Override
    public String toString() {
        return getName()+" "+getSurname()+"("+getAge()+"/"+getShoeSize()+") === over:" +getOveral()+" club:"+ getClubName();
    }
}
