package Colege;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Colege {
    private static final int maxStudentIdRange=5000;

    HashMap<Integer, Student> studentList;
    ArrayList<Student> sortList;

    public Colege(){
        setStudentList(new HashMap<Integer, Student>());
        setSortList(new ArrayList<Student>());
    }

    private HashMap<Integer, Student> getStudentList() {
        return studentList;
    }

    private void setStudentList(HashMap<Integer, Student> studentList) {
        this.studentList = studentList;
    }

    public ArrayList<Student> getSortList() {
        return sortList;
    }

    public void setSortList(ArrayList<Student> sortList) {
        this.sortList = sortList;
    }

    public void run() {
        for(int it=1; it<20; it++){
            Student tmp=generateFakeStudent(it);

            tmp.assignSudentSubject(Subject.MATH);
            tmp.assignSudentSubject(Subject.POLISH);

            if(it%3==0)tmp.assignSudentSubject(Subject.ART);
            if(it%5==0)tmp.assignSudentSubject(Subject.ETHICS);

            System.out.println("Zapisuję na studia: " + tmp.toString(true));

            getStudentList().put(tmp.getAlbumNo(), tmp);
            getSortList().add(tmp);
        }

        System.out.println("\n\n--- <## Posortowana lista studentów po nazwisku ##> ---\n");
        Collections.sort(getSortList());
        for(Student student:getSortList()){
            System.out.println(student);
            System.out.println(student.getStudentSubjects());
        }

        System.out.println("\n\n--- <## Posortowana lista studentów po nazwisku ##> ---\n");
        System.out.println(getSortList().get(0).toString(true));

        giveStudentAMark(getSortList().get(0), Subject.MATH, true, 3);
        System.out.println(getSortList().get(0).getStudentmissingMarks());

        giveStudentAMark(getSortList().get(0), Subject.MATH, false, 5);
        giveStudentAMark(getSortList().get(0), Subject.POLISH, true, 2);
        System.out.println(getSortList().get(0).getStudentmissingMarks());
        giveStudentAMark(getSortList().get(0), Subject.POLISH, false, 4);

        giveStudentAMark(getSortList().get(0), Subject.ETHICS, true, 3);

        System.out.println(getSortList().get(0).toString(true));
        System.out.println(getSortList().get(0).getStudentmissingMarks());

        System.out.println(getSortList().get(0).getStudentSubjects());
        System.out.println(getSortList().get(0).getMarksAverage(false));
        System.out.println(getSortList().get(0).getMarksAverage(true));

        Collections.sort(getSortList(), new StudentAlbumNoComparator());
        System.out.println("\n\n--- <## Posortowana lista studentów po numerze albumu ##> ---\n");
        for(Student student:getSortList()){
            System.out.println(student);
        }

        giveStudentAMark(getSortList().get(7), Subject.MATH, false, 5);
        giveStudentAMark(getSortList().get(14), Subject.MATH, false, 5);

        Collections.sort(getSortList(), new StudentMarkAvgComparator());
        System.out.println("\n\n--- <## Posortowana lista studentów po średniej ocen ##> ---\n");
        for(Student student:getSortList()){
            System.out.println(student+"-"+student.getMarksAverage(false));
        }
    }

    private int generateNewAlbumNo(){
        Random rand=new Random();
        Integer newAlbumNo;
        do{
            newAlbumNo=5000+rand.nextInt(maxStudentIdRange);
        }while(getStudentList().containsKey(newAlbumNo));

        return newAlbumNo;
    }

    private Student generateFakeStudent(int iterator){
        Random rand=new Random();

        return new Student(
                "Fakeimie"+iterator,
                "Fakenazwisko"+iterator,
                generateNewAlbumNo(),
                20+rand.nextInt(5)
        );
    }

    void giveStudentAMark(Student student, Subject subject, boolean lab, int mark){
        try{
            student.setStudentMark(subject, lab, mark);
        }catch(StudentException ex){
            System.out.println("Błąd podczas przypiywania oceny!" + ex.getMessage() + "[" + ex.getStudentId() + "]");
        }
    }
}
