package Colege;

import javafx.util.Pair;

import java.util.HashMap;

public class Student implements Comparable<Student>{
    String name, surname;
    int age;
    Integer albumNo;
    HashMap<Subject, Pair<Integer, Integer>> marks;


    public Student(String name, String surname, int albumNo, int age) {
        setName(name);
        setSurname(surname);
        setAlbumNo(albumNo);
        setAge(age);
        marks=new HashMap<Subject, Pair<Integer, Integer>>();
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private String getSurname() {
        return surname;
    }

    private void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAlbumNo() {
        return albumNo;
    }

    private void setAlbumNo(Integer albumNo) {
        this.albumNo = albumNo;
    }

    private int getAge() {
        return age;
    }

    private void setAge(int age) {
        this.age = age;
    }

    public HashMap<Subject, Pair<Integer, Integer>> getMarks() {
        return marks;
    }

    public void setMarks(HashMap<Subject, Pair<Integer, Integer>> marks) {
        this.marks = marks;
    }

    @Override
    public String toString() {
        return getName()+' ' +getSurname()+'('+getAge()+ "): " +getAlbumNo();
    }

    public String toString(boolean full){
        String result=toString();

        for(Subject subject: getMarks().keySet()){
            result+="\n"+subject+" <L: "+handleMark(getMarks().get(subject).getKey()) +" Ex: "+handleMark(getMarks().get(subject).getValue())+" >";
        }

        return result;
    }

    private String handleMark(Integer mark){
        if(mark!=null)return mark.toString();
        return "-";
    }

    public void assignSudentSubject(Subject subject){
        getMarks().put(subject, new Pair<>(null, null));
    }

    @Override
    public int compareTo(Student o) {
        return getSurname().compareToIgnoreCase(o.getSurname());
    }

    public String getStudentSubjects(){
        return getMarks().keySet().toString();
    }

    public void setStudentMark(Subject subcjet, boolean labs, Integer mark) throws StudentException{
        if(!getMarks().containsKey(subcjet))throw new StudentException("Student nie chodzi na przedmiot: "+subcjet, getAlbumNo());

        Integer lab, exam;
        lab=getMarks().get(subcjet).getKey();
        exam=getMarks().get(subcjet).getValue();

        if(labs)lab=mark;
        else exam=mark;

        getMarks().put(subcjet, new Pair<>(lab, exam));
    }

    public Double getMarksAverage(boolean weighted){
        Double avg=0d;
        int cnt=0;

        for(Subject subject:getMarks().keySet()){
            Integer tmp=getMarks().get(subject).getKey();
            if(tmp!=null){
                if(weighted)avg+=(tmp*subject.getPoints());
                else avg+=tmp;
                cnt++;
            }

            tmp=getMarks().get(subject).getValue();
            if(tmp!=null){
                if(weighted)avg+=(tmp*subject.getPoints());
                else avg+=tmp;
                cnt++;
            }
        }

        if(cnt==0)return 0d;
        else return avg/cnt;
    }

    public String getStudentmissingMarks(){
        String result="";

        for(Subject subject:getMarks().keySet()){
            String tmp="";

            if(getMarks().get(subject).getKey()==null){
                tmp+="lab";
            }

            if(getMarks().get(subject).getValue()==null){
                if(tmp.length()>0)tmp+=", ";
                tmp+="exam";
            }

            if(tmp.length()>0){
                if(result.length()>0)result+=", ";
                result+=subject+"["+tmp+"]";
            }
        }

        return result;
    }
}
