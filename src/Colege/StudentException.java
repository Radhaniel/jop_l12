package Colege;

public class StudentException extends Throwable {
    Integer studentId;

    public StudentException(String message, Integer studentId) {
        super(message);
        setStudentId(studentId);
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return super.toString()+" Numer albumu studenta: "+getStudentId();
    }
}
