package Colege;

import java.util.Comparator;

public class StudentMarkAvgComparator implements Comparator <Student>{
    @Override
    public int compare(Student o1, Student o2) {
        return o2.getMarksAverage(false).compareTo(o1.getMarksAverage(false));

    }
}
