package Colege;

import java.util.Comparator;

public class StudentAlbumNoComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getAlbumNo().compareTo(o2.getAlbumNo());
    }
}
