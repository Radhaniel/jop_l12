package Colege;

public enum Subject {
    MATH("Math", 3),
    POLISH("Polish", 2),
    ART("Art", 1),
    ETHICS("Ethics", 1);

    private String name;
    int points;

    Subject(String name, int points) {
        setName(name);
        setPoints(points);
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    private void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return getName()+"("+getPoints()+")";
    }
}
