import java.util.Comparator;

public class PlayerClubComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {
        return o1.getClubName().compareToIgnoreCase(o2.getClubName());
    }
}
