import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;

public class PlayerLeauge {
    LinkedList<Player> list;

    public PlayerLeauge(){
        list=new LinkedList<>();
    }

    public void doMagic(){
        Random rand=new Random();

        for(int it=5; it>0; it--)list.add(new Player("imie"+it, "nazwisko"+it, 30+rand.nextInt(20), 30+it));

        list.get(1).setClubName("Warka");
        list.get(2).setClubName("Pogoń");
        list.get(3).setClubName("Wisła");

        list.get(2).setShoeSize(12.5f);
        list.get(3).setShoeSize(8.5f);
        list.get(4).setShoeSize(13.5f);

        for(Player pl: list){
            System.out.println(pl);
        }

        System.out.println("<-------------------------->");
        Collections.sort(list);

        for(Player pl: list){
            System.out.println(pl);
        }

        System.out.println("Sortowanie po wieku");
        Collections.sort(list, new PlayerAgeComparator());
        for(Player pl: list){
            System.out.println(pl);
        }

        System.out.println("Sortowanie po klubie");
        Collections.sort(list, new PlayerClubComparator());
        for(Player pl: list){
            System.out.println(pl);
        }

        System.out.println("Sortowanie po rozmiarze buta");
        Collections.sort(list, new PlayerShoeSizeComparator());
        for(Player pl: list){
            System.out.println(pl);
        }


    }
}
